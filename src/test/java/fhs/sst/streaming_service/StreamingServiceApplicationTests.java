package fhs.sst.streaming_service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class StreamingServiceApplicationTests {

    @Test
    void contextLoads() {
        Assertions.assertTrue(true);
    }

}
