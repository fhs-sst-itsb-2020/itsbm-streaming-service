package fhs.sst.streaming_service.controller;

import fhs.sst.streaming_service.service.streaming.StreamingService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * Exposes the streaming REST- API and handles client requests.
 */
@AllArgsConstructor
@RequestMapping(value = "/stream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
@RestController
public class StreamingController {

    public static final String PACKAGE_NAME = "stream";

    private final StreamingService streamingService;

    /**
     * Returns the requested part of the given song.
     * @param songId id of the song to be streamed.
     * @param httpRangeList range of the desired song in bytes.
     * @return requested part of the song
     */
    @GetMapping("/{songId}")
    public Mono<ResponseEntity<byte[]>> stream(@PathVariable Long songId,
                                               @RequestHeader(value = "Range", required = false) String httpRangeList) {
        return Mono.just(streamingService.getSong(songId, httpRangeList));
    }

}
