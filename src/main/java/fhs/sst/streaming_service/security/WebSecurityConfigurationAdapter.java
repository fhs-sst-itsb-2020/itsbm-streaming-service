package fhs.sst.streaming_service.security;

import static fhs.sst.streaming_service.constant.Authorization.ROLE_ADMIN;
import static fhs.sst.streaming_service.constant.Authorization.ROLE_USER;

import fhs.sst.streaming_service.controller.StreamingController;
import fhs.sst.streaming_service.security.filter.JwtRequestFilter;
import lombok.AllArgsConstructor;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Security Configuration for the streaming service
 */
@AllArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

    private final JwtRequestFilter jwtRequestFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
        http.authorizeRequests().mvcMatchers("/" + StreamingController.PACKAGE_NAME + "/**")
                .hasAnyAuthority(ROLE_ADMIN, ROLE_USER);
    }

}