package fhs.sst.streaming_service.constant;

public class Authorization {

    private Authorization() {
        throw new IllegalStateException("Authorization class");
    }

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

}
