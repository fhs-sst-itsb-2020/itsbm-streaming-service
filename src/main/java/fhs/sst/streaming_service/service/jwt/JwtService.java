package fhs.sst.streaming_service.service.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

/**
 * Service to check and parse JWT Tokens
 */
public interface JwtService {

    /**
     * validates and parses the jwt token with the public key
     * @param jwt the jwt token to validate and parse
     * @return the parsed claims
     */
    Jws<Claims> parseAndValidate(String jwt);

}

