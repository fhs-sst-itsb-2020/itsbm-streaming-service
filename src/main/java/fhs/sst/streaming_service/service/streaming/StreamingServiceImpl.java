package fhs.sst.streaming_service.service.streaming;

import fhs.sst.streaming_service.model.Song;
import fhs.sst.streaming_service.component.http.HttpHeaderComponentFactory;
import fhs.sst.streaming_service.component.http.SongState;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.io.FileNotFoundException;
import java.util.Base64;
import java.util.Optional;

@Service
public class StreamingServiceImpl implements StreamingService {

    private final String mediaStorageUrl;
    private final RestTemplate restTemplate;
    private final HttpHeaderComponentFactory httpHeaderComponentFactory;

    public StreamingServiceImpl(@Value("${media-storage.api.url}") String mediaStorageUrl, 
    RestTemplate restTemplate, HttpHeaderComponentFactory httpHeaderComponentFactory) {
        this.mediaStorageUrl = mediaStorageUrl;
        this.restTemplate = restTemplate;
        this.httpHeaderComponentFactory = httpHeaderComponentFactory;
    }

    @Override
    @SneakyThrows
    public ResponseEntity<byte[]> getSong(Long songId, String range) {
        byte[] songBinary = getSongBinaryById(songId);
        return prepareContent(songBinary, range);
    }

    /**
     * Gets the requested song from the Mediastorage Service
     *
     * @param id id of the song
     * @return the song as a byte array
     */
    @SneakyThrows
    private byte[] getSongBinaryById(Long id) {
        ResponseEntity<Song> responseEntity = restTemplate.getForEntity(mediaStorageUrl + id, Song.class);

        Optional<Song> song = Optional.ofNullable(responseEntity.getBody());
        if (song.isPresent()) {
            return Base64.getDecoder().decode(song.get().getBlobString());
        }
        throw new FileNotFoundException("Song with id: " + id + " not found");
    }

    /**
     * Extracts the requested range of bytes from the song binary.
     *
     * @param songBinary byte array of the song
     * @param range      requested range of bytes, if the argument is null, the entire song will be returned
     * @return a ResponseEntity containing the requested range of bytes from the song.
     */
    private ResponseEntity<byte[]> prepareContent(byte[] songBinary, String range) {
        int fileSize = songBinary.length;
        SongState songState = getRequestedRangeState(range);

        switch (songState) {
            case FULL:
                return httpHeaderComponentFactory.getHttpHeaderByState(songState)
                        .createResponseFromSong(fileSize, Tuples.of(0L, Long.MAX_VALUE), songBinary.length - 1, songBinary);
            case PARTIAL:
                return getPartialSong(songBinary, range, fileSize, songState);
            default:
                throw new IllegalArgumentException(String.format("%s is not a valid state for SongState", songState.toString()));
        }
    }

    private ResponseEntity<byte[]> getPartialSong(byte[] songBinary, String range, int fileSize, SongState songState) {
        Tuple2<Long, Long> rangeInterval = parseRangeInterval(range, fileSize);
        int contentLength = (int) ((rangeInterval.getT2() - rangeInterval.getT1()) + 1);
        byte[] partialSong = getPartialSong(songBinary, rangeInterval.getT1(), contentLength);

        return httpHeaderComponentFactory.getHttpHeaderByState(songState)
                .createResponseFromSong(fileSize, rangeInterval, contentLength, partialSong);
    }

    private SongState getRequestedRangeState(String range) {
        if (Optional.ofNullable(range).isEmpty()) {
            return SongState.FULL;
        } else {
            return SongState.PARTIAL;
        }
    }

    /**
     * Parses the range from the string
     *
     * @param range    string containing the range of bytes e.g. 123-4000
     * @param fileSize size of the requested file. if the upper bound in the range is higher than filesize, filesize is used instead
     * @return A tuple containing of the form (lower, higher) - end of the given range
     */
    private Tuple2<Long, Long> parseRangeInterval(String range, int fileSize) {
        String[] rangeInterval = range.split("-");
        long lowerBound = Long.parseLong(rangeInterval[0].substring(6));
        long upperBound = (rangeInterval.length > 1 && fileSize < Long.parseLong(rangeInterval[1]))
                ? Long.parseLong(rangeInterval[1]) : fileSize - 1;
        return Tuples.of(lowerBound, upperBound);
    }

    private byte[] getPartialSong(byte[] songBinary, long lowerBound, int contentLength) {
        byte[] result = new byte[contentLength];
        System.arraycopy(songBinary, (int) lowerBound, result, 0, result.length);
        return result;
    }

}
