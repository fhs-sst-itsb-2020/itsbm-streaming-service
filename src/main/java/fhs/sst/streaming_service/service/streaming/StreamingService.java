package fhs.sst.streaming_service.service.streaming;

import org.springframework.http.ResponseEntity;

/**
 * Handles the main logic of fetching the song from the Data Storage Service and extracting the requested slice from the binary of the song.
 */
public interface StreamingService {

    /**
     * Simple API method to get a range of bytes of the given song.
     * @param songId id of the song in the data-storage service
     * @param range range of bytes of the song (slice)
     * @return the slice of bytes of the song
     */
    ResponseEntity<byte[]> getSong(Long songId, String range);

}
