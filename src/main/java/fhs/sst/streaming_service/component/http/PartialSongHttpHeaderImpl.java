package fhs.sst.streaming_service.component.http;

import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.HttpHeaders.CONTENT_RANGE;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.util.function.Tuple2;

@Component
public class PartialSongHttpHeaderImpl implements HttpHeaderComponent {

    @Override
    public ResponseEntity<byte[]> createResponseFromSong(int fileSize, Tuple2<Long, Long> rangeInterval, int contentLength, byte[] song) {
        StringBuilder contentRange = new StringBuilder()
                .append("bytes ")
                .append(rangeInterval.getT1() )
                .append("-")
                .append(rangeInterval.getT2())
                .append("/" + fileSize);

        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                .header(CONTENT_TYPE, "audio/mp3")
                .header(ACCEPT_RANGES, "bytes")
                .header(CONTENT_LENGTH, String.valueOf(contentLength))
                .header(CONTENT_RANGE, contentRange.toString())
                .body(song);
    }
}
