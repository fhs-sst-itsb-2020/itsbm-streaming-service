package fhs.sst.streaming_service.component.http;

import org.springframework.stereotype.Component;

@Component
public class HttpHeaderComponentFactory {

    private final PartialSongHttpHeaderImpl partialSongHttpHeaderImpl;
    private final FullSongHttpHeaderImpl fullSongHttpHeaderImpl;

    public HttpHeaderComponentFactory(FullSongHttpHeaderImpl fullSongHttpHeaderImpl,
                                      PartialSongHttpHeaderImpl partialSongHttpHeaderImpl) {
        this.partialSongHttpHeaderImpl = partialSongHttpHeaderImpl;
        this.fullSongHttpHeaderImpl = fullSongHttpHeaderImpl;
    }

    public HttpHeaderComponent getHttpHeaderByState(SongState state){
        switch (state) {
            case FULL:
                return fullSongHttpHeaderImpl;
            case PARTIAL:
                return partialSongHttpHeaderImpl;
            default:
                throw new IllegalArgumentException(String.format("%s is not a valid state for class SongState!", state.toString()));
        }
    }
}
