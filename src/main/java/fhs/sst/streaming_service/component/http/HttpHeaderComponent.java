package fhs.sst.streaming_service.component.http;

import org.springframework.http.ResponseEntity;
import reactor.util.function.Tuple2;

public interface HttpHeaderComponent {
    ResponseEntity<byte[]> createResponseFromSong(int fileSize, Tuple2<Long, Long> rangeInterval, int contentLength, byte[] song);
}
