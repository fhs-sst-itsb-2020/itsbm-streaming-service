package fhs.sst.streaming_service.component.http;

public enum SongState {
    FULL,
    PARTIAL;
}
