package fhs.sst.streaming_service.component.http;

import static org.springframework.http.HttpHeaders.CONTENT_LENGTH;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.util.function.Tuple2;

@Component
public class FullSongHttpHeaderImpl implements HttpHeaderComponent {

    @Override
    public ResponseEntity<byte[]> createResponseFromSong(int fileSize, Tuple2<Long, Long> rangeInterval, int contentLength, byte[] song) {
        return ResponseEntity.status(HttpStatus.OK)
                .header(CONTENT_TYPE, "audio/mp3")
                .header(CONTENT_LENGTH, String.valueOf(fileSize - 1))
                .body(song);
    }

}
