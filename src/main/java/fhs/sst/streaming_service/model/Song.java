package fhs.sst.streaming_service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Song data holding class.
 */
@Getter
@NoArgsConstructor(force = true)
@SuperBuilder(toBuilder = true)
public class Song {

    private final Long id;
    private final String blobString;
}
