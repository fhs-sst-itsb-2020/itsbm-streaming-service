# StreamingService

Streamt jeweils einen Song

## Doku

Der Streamingservice erhält vom Frontend ein Request mit einer Song ID und einer Byterange (z.B. 0 – 1000, 1000 – 2000, etc.). <br>
Der Streamingservice sendet einen Request mit der Song ID zum Mediaservice, der das Binary des entsprechenden Songs zurückschickt. <br>
Danach wird diese requestet Byterange von der Binary Datei extrahiert und sendet nur diese ans Frontend zurück.<br> Wenn keine Byterange spezifiziert wurde, wird der gesamte Song an das Frontend geschickt. 

## Sequenzdiagramm
![alt text](https://i.ibb.co/RyX8kMk/Stream-Song.jpg "Sequenzdiagramm streaming")

## Setup 
Anleitung um den Docker Container zu bilden
``` 
docker build -t StreamingService:latest 
```
To run the service:
```
docker run -p 8080:8080  StreamingService:latest
```

## Rest Api
### Get Request
* **Stream song:**
``` 
 http://localhost:8080/stream/{id} 
```

